/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.jhipster.web.rest.vm;
